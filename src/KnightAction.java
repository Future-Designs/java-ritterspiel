import java.util.Iterator;

/**
 * class for actions explizit regarding knights
 * actions contains informations about different states of knight objects
 */
public class KnightAction extends Action
{
    private KnightList knightList;
    private KnightList knightSnapshotList;
    private int duellID;
    private int roundID;
    private int playerID;
    private boolean visible;
    private boolean newLine;

    /**
     * constructor
     * @param KnightList knightList - list of knight objects which affects the action
     * @param int playerID - playerID to assign action to the right knight
     */
    public KnightAction(KnightList knightList, int playerID)
    {
        super();
        this.knightList = knightList;
        knightSnapshotList = new KnightList();
        visible = true;
        newLine = false;
        
        setRoundID();
        setDuellID();
        setPlayerID(playerID);
        
        setPlayerSnapshot();
        setActionString();
    }

    /**
     * setter for knightList
     * @param KnightList knightList - list of knight objects which affects the action
     */
    protected void setPlayer(KnightList knightList)
    {
        this.knightList = knightList;
    }

    /**
     * getter for knightList
     * @return KnighList knightList
     */
    public KnightList getPlayer()
    {
        if(knightList != null) {
            return knightList;
        } else {
            System.out.println("Fehler ! KnightList darf nicht null sein.");
            return null;
        }
    }

    /**
     * getter for knightSnapshotList
     * @return KnightSnapshotList knightSnapshotList
     */
    public KnightList getStaticPlayer()
    {
        if(knightSnapshotList == null) {
            System.out.println("Fehler: getStaticPlayer() in class(KnightAction) knightSnapshotList ist null.");
        }
        return knightSnapshotList;
    }

    /**
     * setter for DuellID
     * iterates over knightList to get actual duellID
     */
    protected void setDuellID()
    {
        Iterator it = knightList.iterator();
        Knight lastKnight = null;
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            if(lastKnight != null) {
                if(knight.getDuellID() == lastKnight.getDuellID()) {
                    duellID = knight.getDuellID();
                } else {
                    System.out.println("Fehler ! Duell-IDs in class(KnightAction) der Spieler passen nicht.");
                }
            }
            lastKnight = knight;
            
        }
    }

    /**
     * getter for duellID
     * @return int duellID
     */
    public int getDuellID()
    {
        return duellID;
    }

    /**
     * setter for roundID
     * iterates over knightList to get actual roundID
     */
    protected void setRoundID()
    {
        Iterator it = knightList.iterator();
        Knight lastKnight = null;
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            if(lastKnight != null) {
                if(knight.getRoundID() == lastKnight.getRoundID()) {
                    roundID = knight.getRoundID();
                } else {
                    System.out.println("Fehler ! Round-IDs in class(KnightAction) der Spieler passen nicht.");
                }
            }
            lastKnight = knight;
            
        }
    }

    /**
     * getter for roundID
     * @return int roundID
     */
    public int getRoundID()
    {
        return roundID;
    }

    /**
     * function for setting a new knightList with static snapshots of the knights in the knightList
     * all attributes of the knightSnapshots are static and represent the status at the time the action occurs
     */
    protected void setPlayerSnapshot()
    {
        if(knightList != null) {  
            Iterator it = knightList.iterator();
            while(it.hasNext()) {
                Knight knight = (Knight)it.next();
                knightSnapshotList.addElement(knight.copy());
            }
        } else {
            System.out.println("Fehler: setPlayerSnapshot in class(KnightAction) konnte nicht erstellt werden, knightList ist null.");
        }
    }

    /**
     * setter for playerID
     * @param int playerID
     */
    protected void setPlayerID(int playerID)
    {
        this.playerID = playerID;
    }

    /**
     * getter for playerID
     * @return int playerID
     */
    public int getPlayerID()
    {
        return playerID;
    }
    
    /**
     * setter for visible variable
     * variable for enable output of action
     */
    protected void setVisible(boolean visible)
    {
        this.visible = visible;
    }
    
    /**
     * getter for visible variable
     * variable for en/dis/able output of action
     */
    protected boolean getVisible()
    {
        return visible;
    }
    
    /**
     * setter for newLine variable
     * variable for enable line break
     */
    protected void setNewLine(boolean newLine)
    {
        this.newLine = newLine;
    }
    
    /**
     * getter for newLine variable
     * variable for en/dis/able line break
     */
    protected boolean getNewLine()
    {
        return newLine;
    }
}
