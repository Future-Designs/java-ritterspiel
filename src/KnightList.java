import java.util.Iterator;

/**
 * class for list of knight objects
 */
public class KnightList extends PlayerList
{
    private int countAlive;

    /**
     * constructor
     */
    public KnightList()
    {
       countAlive = 0;
    }

    /**
     * factory for knightList
     * adds n knight objects to knightList depending how many knight names are given
     * @param String[] knightNames
     * @return list object knightList
     */
    public static KnightList factory(String[] knightNames)
    {
        KnightList knightList = new KnightList();
        //debug
        //System.out.println("Anzahl Namen: " + knightNames.length);
        
        for(int i = 0; i < knightNames.length; i++) {
            knightList.addElement(new Knight(knightNames[i]));
        }
        return knightList;
    }

    /**
     * factory for knightList with 4 knight objects
     * builds new knightList with 4 knights
     * @return
     */
    public static KnightList factoryTest()
    {
        String[] knightNames = new String[4];
        knightNames[0] = "Hugo";
        knightNames[1] = "Emil";
        knightNames[2] = "Kurt";
        knightNames[3] = "Siegfried";
        
        return KnightList.factory(knightNames);
    }

    /**
     * getter for alive player(knights)
     * iterates over knight objects and get alive knights
     * ATTENTION - resets points and tools before returning new list
     * @return list object knightList
     */
    public KnightList getAlivePlayer(boolean toolReset)
    {
        KnightList knightList = new KnightList();
        Iterator<Knight> it = this.iterator();
        while(it.hasNext()) {
            Knight knight = it.next();
            if(knight.getAlive()) {
                knight.resetPoints();   // every round full points
                if(toolReset == true) {
                    knight.resetToolList(); // every round new tools
                }
                knightList.addElement(knight);
            }
        }
        return knightList;
    }

    /**
     * getter for value of alive player(knights)
     * sets variable countAlive
     * @return int value of alive player
     */
    public int getCountAlive()
    {
        Iterator<Knight> it = this.iterator();
        while(it.hasNext()) {
            Knight knight = it.next();
            if(knight.getAlive()) {
                countAlive++;
            }
        }
        this.countAlive = countAlive;
        return this.countAlive;
    }

    /**
     * getter for player depending on playerID
     * iterates over knight objects until one matches playerID
     * @param int playerID - playerID to search for
     * @return object knigth
     */
    public Knight getIDPlayer(int playerID)
    {
        // debug
        //System.out.println(playerID);

        Iterator it = this.iterator();
        int i = 0;
        Knight foundPlayer = null;
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();

            // debug
            //System.out.println("PlayerIDs: " + player.getPlayerID() + " : " + playerID);
            if(knight.getPlayerID() == playerID) {
                foundPlayer = knight;
                i++;
            }
        }
        
        if(i <= 0) {
            System.out.println("Fehler: Kein Player mit der ID " + playerID + " gefunden.");
            return null;
        } else if(i > 1) {
            System.out.println("Fehler: Mehr als ein Player mit der ID " + playerID + " gefunden.");
            return null;
        } else {
            return foundPlayer;
        }
    }

    /**
     * getter for random object
     * iterates over knight objects in list
     * ATTENTION - removes object from list when called
     * @return
     */
    public Knight getRandomObject()
    {
        Knight tempObject = null;
        Iterator<Knight> it = this.iterator();
        if(it.hasNext()) {
            for(int i = 0; i <= getRand().getRandomInt(this.getObjectCount()); i++) {
                tempObject = it.next();
                
            }
            it.remove();
            return tempObject;
        } else {
            System.out.println("No elements in List to iterate.");
            return null;
        }
    }
    
    /**
     * factory method for CustomList
     * used for getting the right child type of object in parent copy method
     * @return Object KnightList
     */
    public KnightList factory()
    {
        KnightList newList = new KnightList();
        
        Iterator<Knight> it = this.iterator();
        while(it.hasNext()) {
            Knight knight = (Knight)it.next().copy();
            newList.addElement(knight);
        }
        
        return newList;
    }
    
    /**
     * knightNames resetter
     */
    public void resetKnightNames()
    {
        Iterator<Knight> it = this.iterator();
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            knight.setName(knight.getTempName());
        }
    }
}
