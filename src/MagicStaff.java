/**
 * class for specific tool MagicStaff
 */
public class MagicStaff extends KnightTool
{
    /**
     * constructor
     */
    public MagicStaff()
    {
        super();
        setCritMult(100);
        setName("Magic-Staff");
        setDescription("vernichtet einen Gegner");
    }
    
    /**
     * factory method
     * @return object MagicStaff
     */
    public MagicStaff factory()
    {
        return new MagicStaff();
    }
    
    /**
     * function to enable tool usage random
     * @return boolean
     */
    public boolean toolControl()
    {
        if(getRandGen().getRandomInt(99) == 50) {
            return true;
        } else {
            return false;
        }
    }
}