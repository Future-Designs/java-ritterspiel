/**
 * class for specific list of duellSim objects
 */
public class KnightDuellList extends DuellList
{
    /**
     * constructor
     */
    public KnightDuellList()
    {
        super();
    }

    /**
     * factory for KnigthDuellList
     * builds new list with one duell for each 2 knights
     * ATTENTION - calls getRandPlayer() to get list with two random knights
     * @param list object knightList
     * @param list object knightActionList
     * @param boolean detailed - value for option to log detailed informations
     * @return list object knightDuellList
     */
    public static KnightDuellList factory(KnightList knightList, KnightActionList knightActionList, boolean detailed)
    {
        KnightDuellList knightDuellList;
        knightDuellList = new KnightDuellList(); // Änderung vorgenommen
        while(knightList.getObjectCount() >= 2) {
            
            //debug
            //System.out.println("Knight-List-Objekte: " + knightList.getObjectCount());
            //System.out.println("Versuche 2 Objekte zu entfernen und eine DuellSim zu erstellen.");
            
            System.out.println();
            knightDuellList.addElement(new KnightDuellSim(getRandPlayer(2, knightList), knightActionList, detailed));
        }
        
        return knightDuellList;
    }

    /**
     * function for getting a random knight list
     * calls getRandomObject() in knightList to get random knight
     * @param int value - number of knights per list
     * @param list object knightList - list with knight objects
     * @return list object newKnightList - list with n(param) knights
     */
    private static KnightList getRandPlayer(int value, KnightList knightList)
    {
        KnightList newKnightList = new KnightList();
        for(int i = 0; i < value; i++) {
            Knight knight = (Knight)knightList.getRandomObject();
            knight.setPlayerID(i);
            newKnightList.addElement(knight);
        }
        return newKnightList;
    }
}