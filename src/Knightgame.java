import java.util.Iterator;

/**
 * class for controlling KnightGame with all modular functions
 */
public class Knightgame extends Game
{
    // option for GUI enable/disable
    private final boolean GUI;
    // option for detailed logging enable/disable
    private final boolean DETAILS;
    // option for resetting Tools afer each duell
    private final boolean TOOLRESET;

    private KnightInputParser parser;
    private KnightRoundSim knightRoundSim;
    private KnightList knightList;
    private KnightActionList knightActionList;
    private KnightConsoleOut consoleOut;

    private int round;

    /**
     * constructor
     */
    public Knightgame()
    {
        super();
        
        // option for GUI - not implemented
        GUI = false;
        // option for detailed output
        DETAILS = true;
        // option for resetting Tools
        TOOLRESET = true;
        
        round = 1;
        parser = new KnightInputParser();
        
        // productive mode
        knightList = KnightList.factory(parser.getAllPlayer());
        
        // test mode
        //knightList = KnightList.factory(parser.getDummyPlayer());
        knightActionList = new KnightActionList();
        consoleOut = new KnightConsoleOut();
        
        run();
    }

    /**
     * main method for independend programm-start
     * @param args
     */
    public static void main(String[] args)
    {
        new Knightgame();
    }

    /**
     * central controlling of components
     * while more than 2 knight objects are alive, it performs new rounds
     * calls as last action the output-function for text-based console-outprint
     */
    private void run()
    {
        knightRoundSim = new KnightRoundSim(knightActionList, GUI, DETAILS); //GUI boolean gui activated

        while(knightList.getCountAlive() >= 2) {
            knightRoundSim.prepareRound(knightList, DETAILS);
            knightRoundSim.performRound(GUI, DETAILS);//GUI boolean gui activated
            this.knightList = knightList();
            round++;
        }
        
        outputRoundStatus();
    }

    /**
     * output function
     * iterates over actionList and prints logged actions text-based out
     */
    private void outputRoundStatus()
    {
        consoleOut.outputAllRounds(knightActionList, parser);
        consoleOut.outputWinner(knightActionList);
    }

    /**
     * getter for new knightList
     * creates a new knightList with all alive knights for preparing a new round
     * @return list object knightList
     */
    public KnightList knightList()
    {
        if(knightRoundSim.getRoundID() > 1) {
            return knightRoundSim.getPlayerList().getAlivePlayer(TOOLRESET);
        } else {
            return this.knightList;
        }
    }
}
