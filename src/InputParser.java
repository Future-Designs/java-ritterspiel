import java.util.Scanner;

/**
 * class for scanning and parsing console input
 * uses eventer for sleeping threads
 */
public class InputParser
{
    private Scanner scanner;
    private Eventer eventer;

    /**
     * constructor
     */
    public InputParser()
    {
        scanner = new Scanner(System.in);
        eventer = new Eventer();
    }

    /**
     * scanner for int values
     * @param String command - text shown for command prompt
     * @return int
     */
    public int getInt(String command)
    {
        System.out.println("");
        System.out.println(command);
        System.out.print("> ");
        if(scanner.hasNextInt()) {
            int intInput = scanner.nextInt();
            return intInput;
        } else { 
            return 0;
        }
    }

    /**
     * scanner for string values
     * @param String command - text shown for command prompt
     * @return String or null
     */
    public String getString(String command)
    {
        System.out.println("");
        System.out.println(command);
        System.out.print("> ");
        if(scanner.hasNextLine()) {
            String strInput = scanner.next();
            return strInput;
        } else {
            return null;
        }
        
    }

    /**
     * scanner for command inputs
     * calls interpreter for comparing input with commands
     * @param String command - text shown for command prompt
     */
    public void getStringCommand(String command)
    {
        System.out.println("");
        System.out.println(command);
        showCommands();
        boolean matching = false;
        while(matching != true) {
            System.out.print("> ");
            String input = scanner.next();
            matching = parseCommands(input);
        }
        
    }

    /**
     * function for outprint helptext
     * @param Array helpText - helptext-array
     */
    public void printOutHelp(String[] helpText)
    {
        for(int i = 0; i < helpText.length; i++) {
            System.out.println(helpText[i]);
        }
    }

    /**
     * getter for Eventer object
     *
     * @return object Eventer
     */
    protected Eventer getEventer()
    {
        return eventer;
    }

    /**
     * parser for commands
     * compares parameter input with fix commands
     * @param String input - input string to compare with commands
     * @return boolean
     */
    protected boolean parseCommands(String input)
    {
        return false;
    }

    /**
     * function for showing available commands
     */
    protected void showCommands()
    {
        
    }
}
