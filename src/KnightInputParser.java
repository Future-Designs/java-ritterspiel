import java.util.Scanner;

/**
 * class for scanning and parsing console inputs
 */
public class KnightInputParser extends InputParser
{
    private final String YES = "ja";
    private final String QUIT = "quit";
    private final String HELP = "help";
    private final String KNIGHTS = "Wieviele Ritter sollen antreten ? (2, 4, 8, 16 oder 32)";
    private final String KNIGHTNAME = "Bitte geben Sie den Namen des Ritters ein:";
    private final String[] HELPTEXT;

    /**
     * constructor
     */
    public KnightInputParser()
    {
        super();
        HELPTEXT = getHelpText();
    }

    /**
     * function to get n knight-names as string-array
     * scanns console input for string
     * @return String[]
     */
    public String[] getAllPlayer()
    {
        int maxPlayer = getInt(KNIGHTS);
        while(checkPlayerInt(maxPlayer) != true) {
            maxPlayer = getInt(KNIGHTS);
        }
        String[] allPlayer = new String[maxPlayer];
        for(int i = 0; i < maxPlayer; i++) {
            String tempPlayer = getString(KNIGHTNAME);
            if(tempPlayer != null) {
                allPlayer[i] = tempPlayer;
            } else {
                System.out.println("Fehler: getAllPlayer() in class KnightInputParser");
            }
        }
        return allPlayer;
    }

    private boolean checkPlayerInt(int maxPlayer)
    {
        if(maxPlayer >= 2) {
            if(maxPlayer % 2 == 0) {
                if(maxPlayer <= 32) {
                    switch(maxPlayer) {
                        case 2:
                            return true;
                        case 4:
                            return true;
                        case 8:
                            return true;
                        case 16:
                            return true;
                        case 32:
                            return true;
                        default:
                            System.out.println("Auf Grund des Matchverfahrens sind nur die Zahlen 4, 8, 16, 32 zulässig");
                            return false;
                    }
                } else {
                    System.out.println("Die maximale Ritteranzahl beträgt 32 !");
                    return false;
                }
            } else {
                System.out.println("Bitte eine gerade Anzahl eingeben !");
                return false;
            }
        } else {
            System.out.println("Es werden mindestens 2 Ritter benötigt !");
            return false;
        }
        
    }
    
    /**
     * factory for dummy-player
     * gives a array of player-names for test-mode
     * @return String[]
     */
    public String[] getDummyPlayer()
    {
        String[] dummyPlayer = new String[4];
        dummyPlayer[0] = "Hugo";
        dummyPlayer[1] = "Lanzelot";
        dummyPlayer[2] = "Kunibert";
        dummyPlayer[3] = "Ullrich";
        return dummyPlayer;
    }

    /**
     * parser for commands
     * compares input stream with static commands
     * controls programm-flow depending on input-commands
     * @param String input
     * @return boolean
     */
    protected boolean parseCommands(String input)
    {
        switch(input) {
            case YES:
                return true;
            case QUIT:
                System.out.println("Programm wird beendet...");
                getEventer().threadSleep(1000);
                System.exit(0);
            case HELP:
                printOutHelp(HELPTEXT);
                return false;
            default:
                System.out.println("Der Befehl " + input + " wurde nicht erkannt.");
                printOutHelp(HELPTEXT);
                return false;
        }
    }

    /**
     * getter for helptext
     * static helptext for printout
     * @return String[]
     */
    private String[] getHelpText()
    {
        String[] helptext = new String[4];
        helptext[0] = "";
        helptext[1] = "Folgende Befehle sind verfügbar:";
        helptext[2] = "ja - Programm fortführen || quit - Programm beenden || help - Befehle anzeigen";
        helptext[3] = "";
        return helptext;
    }

    /**
     * function for showing available commands
     * printout commands
     */
    protected void showCommands()
    {
        System.out.println("(ja - weiter || quit - beenden || help - hilfe)");
    }
}
