/**
 * class for KnightTool objects
 * represents tools with specific attributs
 */
public class KnightTool extends Tool
{
    // value for calculating final damage or manipulating damage of knight objects
    private int critMult;
    private RandGen randGen;
    private boolean visible;
    
    /**
     * constructor
     */
    public KnightTool()
    {
        super();
        randGen = new RandGen();
        visible = true;
    }
    
    /**
     * getter for random generator
     * @return Object RandGen
     */
    public RandGen getRandGen()
    {
        return randGen;
    }
    
    /**
     * getter for variable critMult
     * @return int
     */
    public int getCritMult()
    {
        return this.critMult;
    }

    /**
     * setter for variable critMult
     * @param int critMult
     */
    public void setCritMult(int critMult)
    {
        this.critMult = critMult;
    }

    /**
     * getter for variable value
     * used in childclass
     * @return int
     */
    public int getValue(){return 0;}
    
    /**
     * factory method
     * @return object KnightTool
     */
    public KnightTool factory()
    {
        KnightTool newTool = new KnightTool();
        newTool.setCritMult(this.getCritMult());
        newTool.setVisible(this.getVisible());
        return newTool;
    }
    
    /**
     * function to en/dis/able tool usage random
     * @return boolean
     */
    public boolean toolControl()
    {
        if(getRandGen().getRandomInt(3) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * setter for visible variable
     * variable for enable output of action
     */
    protected void setVisible(boolean visible)
    {
        this.visible = visible;
    }
    
    /**
     * getter for visible variable
     * variable for en/dis/able output of action
     */
    protected boolean getVisible()
    {
        return visible;
    }
    
    /**
     * knight manipulator
     * manipulates knight object
     */
    
    public void manipulateKnight(Knight knight)
    {
    }
}