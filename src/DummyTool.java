/**
 * class for dummy tool, placeholder
 */
public class DummyTool extends KnightTool
{
    /**
     * constructor
     */
    public DummyTool()
    {
       super();
       setCritMult(1);
       setName("Dummy-Tool");
       setVisible(false);
    }

    /**
     * getter for value variable
     *
     * @return int
     */
    public int getValue()
    {
        return 0;
    }
    
    /**
     * factory method
     * @return object DummyTool
     */
    public DummyTool factory()
    {
        return new DummyTool();
    }
}