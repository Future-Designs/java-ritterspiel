import java.util.Iterator;

/**
 * class for calulating damage relating to knight objects
 */
public class KnightDamageCalculator extends DamageCalculator
{
    private KnightList knightList;
    private KnightActionList knightActionList;

    // array with base damage of knight objects
    private int[] baseDamage;
    // array with final damage of knight objects (after calculating the influence of tools, crit, etc.)
    int[] playerDamage;
    // array of tool objects relating to knight objects
    private KnightTool[] tools;

    /**
     * constructor
     * @param KnightActionList knightActionList
     */
    public KnightDamageCalculator(KnightActionList knightActionList)
    {
        this.knightActionList = knightActionList;
        
        knightList = new KnightList();
        
        baseDamage = new int[10];
        playerDamage = new int[10];
        
        for(int i = 0; i < baseDamage.length; i++) {
            baseDamage[i] = 0;
        }
        
        tools = new KnightTool[10];
        for(int i = 0; i < tools.length; i++) {
            tools[i] = null;
        }
    }

    /**
     * function for initial configuration
     * calls setter for knightList
     * @param KnightList knightList
     */
    public void configureCalculator(KnightList knightList)
    {
        setPlayer(knightList);
    }

    /**
     * setter for knightList
     * ATTENTION - adds new duellAction to knightActionList
     * @param knightList
     */
    private void setPlayer(KnightList knightList)
    {
        this.knightList = knightList;
        knightActionList.addElement(new KnightDuellAction(this.knightList));
    }

    /**
     * getter for knightList
     * @return KnightList knightList
     */
    public KnightList getPlayer()
    {
        return this.knightList;
    }

    /**
     * setter for tools
     * iterates over knight objects to add theire tools into tools-array
     * ATTENTION - calls getRandomObject() on knight to get one random tool
     * sets activeTool in Knight object
     */
    private void setTools()
    {
        Iterator it = knightList.iterator();
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            KnightTool tool = (KnightTool)knight.getRandomObject();
            tool.manipulateKnight(knight);
            tools[knight.getPlayerID()] = tool;

            // debug
            //System.out.println("DamageCalc -> setTools -> Player " + player.getName() + " -> Tool: " + tool);
        }
    }

    /**
     * setter for baseDamage-array
     * iterates over knight objects to get theire damage
     * ATTENTION - calls getDamage() on knight objects to get random damage
     * sets randomDamage in Knight object
     */
    private void setDamage()
    {
        Iterator it = knightList.iterator();
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            baseDamage[knight.getPlayerID()] = knight.getDamage();
            knightActionList.addElement(new KnightRandAction(knightList, knight.getPlayerID()));
        }
    }

    /**
     * calculation of damage considering tools and theire attributs
     * adds actions for each change on knight object to ActionList
     * @param boolean detailed - boolean value for option to log detailed informations
     */
    public void calc(boolean detailed)
    {
        setTools();
        setDamage();
        
        //System.out.println("playerList objectCount: " + playerList.getObjectCount());
        
        Iterator it = knightList.iterator();
        while(it.hasNext()) {
            Knight knight = (Knight)it.next();
            //System.out.println("Player ID: " +  player.getPlayerID());
            playerDamage[knight.getPlayerID()] = baseDamage[knight.getPlayerID()] * tools[knight.getPlayerID()].getCritMult();
            if(detailed == true) {
                knightActionList.addElement(new KnightToolAction(knightList, knight.getPlayerID()));
            }
        }
        
        playerDamage = checkHeal(playerDamage, detailed);
        playerDamage = checkShield(playerDamage, detailed);
        setPlayerFinalDamage(playerDamage);
        
        knightActionList.addElement(new KnightDamageAction(knightList, detailed));
        
        setPlayerPoints(playerDamage);
    }

    /**
     * shield-tool calculating
     * manipulate damage if one knight object holds a shield (tool) object
     * recalculates the damage of knight objects depending on which is reflecting damage
     * sets new player damage if this changed while calculating
     * @param int[] playerDamage
     * @param boolean detailed - boolean value for option to log detailed informations
     * @return int[] playerDamage
     */
    private int[] checkShield(int[] playerDamage, boolean detailed)
    {
        Knight lastPlayer = null;
        Iterator newIt = knightList.iterator();
        while(newIt.hasNext()) {
            Knight knight = (Knight)newIt.next();
            if(lastPlayer != null) {
                int tempDamage;
                if(playerDamage[knight.getPlayerID()] < 0) {
                    if(detailed == true) {

                        // additional log information
                        // actionList.addElement(new CustomAction(playerList, "reflektiert Schaden", player.getPlayerID()));
                    }
                    tempDamage = playerDamage[lastPlayer.getPlayerID()];
                    playerDamage[lastPlayer.getPlayerID()] = 0;
                    playerDamage[knight.getPlayerID()] = baseDamage[knight.getPlayerID()] + tempDamage;
                }
                
                if(playerDamage[lastPlayer.getPlayerID()] < 0) {
                    if(detailed == true) {

                        // additional log information
                        //actionList.addElement(new CustomAction(playerList, "reflektiert Schaden", player.getPlayerID()));
                    }
                    tempDamage = playerDamage[knight.getPlayerID()];
                    playerDamage[knight.getPlayerID()] = 0;
                    playerDamage[lastPlayer.getPlayerID()] = baseDamage[lastPlayer.getPlayerID()] + tempDamage;
                }
            }
            lastPlayer = knight;
        }
        return playerDamage;
    }

    /**
     * heal-tool calculating
     * manipulating lifepoints of knight objects which hold a heal-tool object
     * increases lifepoints of concerning knight object
     * @param int[] playerDamage - damage-array to recalculate
     * @param boolean detailed - boolean value for option to log detailed informations
     * @return int[] playerDamage - recalculated damage
     */
    private int[] checkHeal(int[] playerDamage, boolean detailed)
    {
        Iterator newIt = knightList.iterator();
        while(newIt.hasNext()) {
            Knight knight = (Knight)newIt.next();
            if(playerDamage[knight.getPlayerID()] == 0) {
                knight.increasePoints(tools[knight.getPlayerID()].getValue());
                playerDamage[knight.getPlayerID()] = baseDamage[knight.getPlayerID()];
                if(detailed == true) {
                    String customString = "heilt sich um ";
                    customString += tools[knight.getPlayerID()].getValue();
                    customString += " Punkte";
                    knightActionList.addElement(new KnightCustomAction(knightList, customString, knight.getPlayerID()));
                }
                
            }
        }
        return playerDamage;
    }

    /**
     * function to set player-points depending on damage-values of each knight object
     * iterates over knigth objects and decrease/increase theire points
     * @param int[] playerDamage
     */
    private void setPlayerPoints(int[] playerDamage)
    {
        Knight lastPlayer = null;
        Iterator newIt = knightList.iterator();
        while(newIt.hasNext()) {
            Knight knight = (Knight)newIt.next();
            if(lastPlayer != null) {
                lastPlayer.decreasePoints(playerDamage[knight.getPlayerID()]);
                lastPlayer.checkAlive();
                if(lastPlayer.getAlive() == true) {
                    knight.decreasePoints(playerDamage[lastPlayer.getPlayerID()]);
                    knight.checkAlive();
                }
            }
            lastPlayer = knight;
        }
    }

    /**
     * setter for finalDamage in knight objects
     * iterates over knight objects to set finalDamage
     * finalDamage represents all damage dealt in one calculation-process
     * @param int[] damage
     */
    private void setPlayerFinalDamage(int[] damage)
    {
        Iterator newIt = knightList.iterator();
        while(newIt.hasNext()) {
            Knight knight = (Knight)newIt.next();
            knight.addFinalDamage(damage[knight.getPlayerID()]);
        }
    }
}
