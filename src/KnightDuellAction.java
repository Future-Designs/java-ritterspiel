/**
 * class for duell actions relating to knight objects
 */
public class KnightDuellAction extends KnightAction
{
    /**
     * constructor
     * @param KnightList knightList
     */
    public KnightDuellAction(KnightList knightList)
    {
        super(knightList, 0);
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString()
    {
        if(getStaticPlayer() != null) {
            String actionString = "";
            actionString += getStaticPlayer().getIDPlayer(1).getName();
            actionString += " kämpft während Runde ";
            actionString += getStaticPlayer().getIDPlayer(1).getRoundID();
            actionString += " gegen ";
            actionString += getStaticPlayer().getIDPlayer(2).getName();
            return actionString;
        } else {
            System.out.println("Fehler: setDuellString in class(DuellAction) playerSnapshotList ist null.");
            return null;
        }
        
    }
}
