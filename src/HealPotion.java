/**
 * class for health-potion object belongs to tools
 * contains heal-amount
 */
public class HealPotion extends KnightTool
{
    private static int HEALVALUE = 20;

    /**
     * constructor
     */
    public HealPotion()
    {
        super();
        setCritMult(0);
        setName("Heal-Potion");
        setDescription("heilt um " + HEALVALUE + " Lebenspunkte");
    }

    /**
     * getter vor value (heal-amount)
     * @return int
     */
    public int getValue()
    {
        return HEALVALUE;
    }
    
    /**
     * factory method
     * @return object HealPotion
     */
    public HealPotion factory()
    {
        return new HealPotion();
    }
}