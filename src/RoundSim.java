/**
 * class for simulating a single round with player objects
 */
public class RoundSim
{
    private int roundID;

    /**
     * constructor
     */
    public RoundSim()
    {
        roundID = 1;
    }

    /**
     * getter for roundID
     * @return int
     */
    public int getRoundID()
    {
        return roundID;
    }

    /**
     * function to increase roundID
     */
    public void plusRoundID()
    {
        roundID++;
    }
}
