import java.util.ArrayList;
import java.util.Iterator;

/**
 * class for encapsulating list functionality from ArrayList
 */
public class CustomList extends ArrayList implements Copyable
{
    private int objectCount;

    /**
     * constructor
     */
    public CustomList()
    {
        
    }

    /**
     * getter vor counter of objects in list
     *
     * calls setter before returning int
     * @return int
     */
    public int getObjectCount()
    {
        checkObjectCount();
        return objectCount;
    }

    /**
     * checks counter of objects in list
     */
    protected void checkObjectCount()
    {
        this.objectCount = this.size();
    }
    
    /**
     * setter for objectCount variable
     * @param int
     */
    public void setObjectCount(int objectCount)
    {
        this.objectCount = objectCount;
    }
    
    /**
     * injector for single objects
     *
     * @param object - object to add to list
     */
    public void addElement(Object object)
    {
        this.add(object);
    }

    /**
     * injector for multiple objects from list
     *
     * @param customList - list with objects to inject
     */
    public void addAllElements(CustomList customList)
    {
        Iterator it = customList.iterator();
        while(it.hasNext()) {
            this.addElement(it.next());
            it.remove();
        }
    }
    
    /**
     * copy method to get a copy of the object with its attributs
     * @return Object
     */
    public CustomList copy()
    {
        CustomList newList = factory();
        newList.setObjectCount(this.getObjectCount());
        return newList;
    }
    
    /**
     * factory method for CustomList
     * used for getting the right child type of object in parent copy method
     * @return Object CustomList
     */
    public CustomList factory()
    {
        return new CustomList();
    }
}
