/**
 * class for morph tool, transfers enemy in animal
 */
public class MagicMorphTool extends KnightTool
{
    /**
     * constructor
     */
    public MagicMorphTool()
    {
       super();
       setCritMult(1);
       setName("Magic-Morph-Tool");
       setDescription("verwandelt Gegner in ein Tier...");
    }

    /**
     * getter for value variable
     *
     * @return int
     */
    public int getValue()
    {
        return 0;
    }
    
    /**
     * factory method
     * @return object DummyTool
     */
    public MagicMorphTool factory()
    {
        return new MagicMorphTool();
    }
    
    /**
     * function to enable tool usage random
     * @return boolean
     */
    public boolean toolControl()
    {
        if(getRandGen().getRandomInt(10) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * knight manipulator
     * manipulates knight object
     */
    
    public void manipulateKnight(Knight knight)
    {
        String name = knight.getClearName();
        knight.setTempName(name);
        
        switch(getRandGen().getRandomInt(3)) {
            case 0:
                knight.setName("Frosch(" + name + ")");
                break;
            case 1:
                knight.setName("Schwein(" + name + ")");
                break;
            case 2:
                knight.setName("Kuh(" + name + ")");
                break;
            case 3:
                knight.setName("Biber(" + name + ")");
                break;
            default:
                break;
        }
    }
}