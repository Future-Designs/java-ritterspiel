/**
 * class for list of player objects
 */
public class PlayerList extends CustomList
{
    private RandGen randGen;

    /**
     * constructor
     */
    public PlayerList()
    {
        randGen = new RandGen();
    }

    /**
     * getter for RandGenerator
     * @return object RandGen
     */
    protected RandGen getRand()
    {
        return randGen;
    }
    
    /**
     * factory method for CustomList
     * used for getting the right child type of object in parent copy method
     * @return Object PlayerList
     */
    public PlayerList factory()
    {
        return new PlayerList();
    }
}
