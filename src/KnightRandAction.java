/**
 * class for random action
 */
public class KnightRandAction extends KnightAction
{
    /**
     * constructor
     * @param list object knightList
     * @param int playerID
     */
    public KnightRandAction(KnightList knightList, int playerID)
    {
        super(knightList, playerID);
    }

    /**
     * function for creating the action string
     * action strings represent a text based form of informations about the action
     * @return String actionString - string with action information
     */
    protected String buildActionString()
    {
        if(getStaticPlayer() != null) {
            String actionString = "";
            actionString += getStaticPlayer().getIDPlayer(getPlayerID()).getName();
            actionString += " würfelt ";
            actionString += getStaticPlayer().getIDPlayer(getPlayerID()).getRandDamage();
            actionString += " Schaden.";
            return actionString;
        } else {
            System.out.println("Fehler: setRandActionString in class(CustomAction) staticPlayer ist null.");
            return null;
        }
    }
}